---
layout: page
title: 
permalink: "/shop/"
---

<div class="nav-link" style="font-family: inherit;" id="myShop">
    <a href="https://rovira-store.myspreadshop.es/all">La Botigueta Online</a>
</div>

<script>
    var spread_shop_config = {
        shopName: 'rovira-store',
        locale: 'es_ES',
        prefix: 'https://rovira-store.myspreadshop.es',
        baseId: 'myShop'
    };
</script>

<script type="text/javascript"
        src="https://rovira-store.myspreadshop.es/shopfiles/shopclient/shopclient.nocache.js">
</script>