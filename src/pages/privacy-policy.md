---
layout: page
title: Política de Privacitat
permalink: /privacy-policy/
---

### 1. Introducció
L'Associació de Veïns i Comerciants de la Plaça Rovira i Trias (en endavant, "nosaltres") es compromet a protegir la privacitat i seguretat de les dades personals dels usuaris del nostre lloc web. Aquesta política explica com recopilem, utilitzem, i protegim la vostra informació personal.

### 2. Responsable del Tractament de les Dades
Nom: Associació de Veïns i Comerciants de la Plaça Rovira i Trias
Adreça: Carrer Providència, 81, baixos
Correu electrònic: hola@colrovirencs.org

### 3. Dades que Recopilem
Podem recopilar les següents dades personals:

* Nom i cognoms
* Adreça de correu electrònic
* Data de neixament
* Número de telèfon
* Altres dades proporcionades pels usuaris a través dels nostres formularis de contacte o d'inscripció.

### 4. Finalitats del Tractament
Les dades personals recopilades es tractaran amb les següents finalitats:

Respondre a les consultes enviades pels usuaris.

Gestionar inscripcions i col·laboracions amb l'Associació.

Enviar comunicacions relacionades amb les activitats i esdeveniments organitzats.

### 5. Base Legal pel Tractament
El tractament de les vostres dades es basa en:

El vostre consentiment explícit, quan ens proporcioneu les vostres dades voluntàriament.

L'execució d'una relació contractual, en cas d'inscripcions o col·laboracions.

### 6. Compartició de les Dades
No compartirem les vostres dades personals amb tercers, excepte quan sigui necessari per complir amb obligacions legals.

###
7. Drets dels Usuaris
Podeu exercir els següents drets en qualsevol moment:

- Accés: Saber quines dades personals tenim sobre vosaltres.
- Rectificació: Sol·licitar la correcció de dades incorrectes o incompletes.
- Supressió: Demanar l'eliminació de les vostres dades personals.
- Oposició: Oposar-vos al tractament de les vostres dades.
- Portabilitat: Rebre les vostres dades personals en un format estructurat.
- Per exercir aquests drets, podeu contactar-nos a través de:
- Correu electrònic: hola@colrovirencs.org

### 8. Conservació de les Dades
Les vostres dades es conservaran únicament durant el temps necessari per complir amb les finalitats descrites en aquesta política o fins que sol·liciteu la seva eliminació.

### 9. Cookies
Aquest lloc web pot utilitzar cookies per millorar l'experiència de navegació. Podeu gestionar la configuració de cookies a través del vostre navegador.

### 10. Modificacions
Ens reservem el dret de modificar aquesta Política de Privacitat en qualsevol moment. Es recomana als usuaris revisar-la periòdicament.

