---
layout: page
title: 
permalink: "/about/"
image: assets/images/screenshot.png
---

Som l’Associació de Veïns i Comerciants de la Plaça Rovira i Trias i les seves rodalies.

Ens encarreguem d’organitzar la Festa Major de Gràcia a la Plaça Rovira i Trias. Sorprenent, oi? 😉

I sí, tu també pots formar part de l’Associació! Hi ha moltes formes de col·laborar, més enllà de ser soci. Vine al nostre local i descobreix com pots sumar-te a aquesta gran família. T’hi esperem amb els braços oberts!