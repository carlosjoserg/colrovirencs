---
layout: page
title: Contacte
permalink: "/contact/"
---

<p class="mb-8">Gràcies pel teu interés.</p>

<p class="mb-8">Ens podràs trobar gairebé tots els <strong>Dimecres entre 20:00h i 22:00h</strong> al nostre local que ara mateix s'ubica al carrer Providència 81 del barri de Gràcia, a Barcelona.</p>

<div style="align-items: center;">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4193.017448317315!2d2.1566436749952347!3d41.4065579118981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a2bbd41eb8a3%3A0x8dd4703a95c49df8!2sAssociaci%C3%B3%20de%20Ve%C3%AFns%20de%20la%20Pla%C3%A7a%20Rovira!5e0!3m2!1sen!2ses!4v1735853143780!5m2!1sen!2ses" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
</div>

<!-- form action="https://formspree.io/{{site.email}}" method="POST">    
<p class="mb-4"></p>Thank you for your interest in getting in touch with us. Please send your message here. We will reply as soon as possible!</p>
<div class="form-group row">
<div class="col-md-6">
<input class="form-control" type="text" name="name" placeholder="Name*" required>
</div>
<div class="col-md-6">
<input class="form-control" type="email" name="_replyto" placeholder="E-mail Address*" required>
</div>
</div>
<textarea rows="8" class="form-control mb-3" name="message" placeholder="Message*" required></textarea>    
<input class="btn btn-dark" type="submit" value="Send">
</form -->